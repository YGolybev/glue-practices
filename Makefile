all: flightDb

flightDb: main.o FlightRepository.o FlightInput.o
	g++ main.o FlightRepository.o FlightInput.o -o flightDb;

main.o: main.cpp
	g++ -c main.cpp;

FlightRepository.o: FlightRepository.cpp
	g++ -c FlightRepository.cpp;

FlightInput.o: FlightInput.cpp
	g++ -c FlightInput.cpp;

clean:
	rm -rf *.o flightDb;

exVal: forValgrind
	./flightDbVal

forValgrind: vmain.o vFlightRepository.o vFlightInput.o
	g++ main.o FlightRepository.o FlightInput.o -O0 -o flightDbVal;

vmain.o: main.cpp
	g++ -c -g -O0 main.cpp;

vFlightRepository.o: FlightRepository.cpp
	g++ -c -g -O0 FlightRepository.cpp;

vFlightInput.o: FlightInput.cpp
	g++ -c -g -O0 FlightInput.cpp;

tests: buildTests
	./testsEx

buildTests: testsFramework FlightRepository.o testsCode
	g++ -std=c++1y TestFlightRepository.o FlightRepository.o -o testsEx

testsCode: TestFlightRepository.cpp
	g++ -c -std=c++1y TestFlightRepository.cpp

testsFramework: catch1.hpp
	g++ -c -std=c++1y catch1.hpp -o catch1.hpp.gch
	