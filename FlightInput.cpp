//
// Created by Юрий Голубев on 15.01.18.
//

#include "FlightInput.hpp"

long inputLong() {
    long input = -1;
    string userInput;

    do {
        getline(cin, userInput);

        try {
            input = stol(userInput);
        } catch (...) {
            cout << "harosh Alexander Igorevich (nado vvodit timestamp)";
        }
    } while (input < 0);

    return input;
}

flight inputNewFlight() {
    getchar();

    flight newFlight;

    cout << "ENTER MANUFACTURER: \n";
    getline(cin, newFlight.planeManufacturer);

    cout << "ENTER ARRIVAL AIRPORT: \n";
    getline(cin, newFlight.arrivalAirport);

    cout << "ENTER DEPARTURE AIRPORT: \n";
    getline(cin, newFlight.departureAirport);

    cout << "ENTER DEPARTURE TIME: \n";
    newFlight.departureTime = inputLong();

    cout << "ENTER ARRIVAL TIME: \n";
    newFlight.arrivalTime = inputLong();

    return newFlight;
};