//
// Created by Юрий Голубев on 15.01.18.
//

#ifndef UNTITLED_FLIGHTREPOSITORY_HPP
#define UNTITLED_FLIGHTREPOSITORY_HPP

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

typedef struct flight {
    long departureTime = -1;
    long arrivalTime = -1;
    std::string departureAirport;
    std::string arrivalAirport;
    std::string planeManufacturer;

    void show();

    bool equ(flight other);
} flight;

class FlightRepository {
private:
    std::string databaseFilename;

public:
    explicit FlightRepository(std::string *dbfile) : databaseFilename(std::move(*dbfile)) {
    }

    void add(flight flight);
    void update(int position, flight flight);
    void remove(int position);
    pair<flight, bool> get(int position);
    const long getSize();

};


#endif //UNTITLED_FLIGHTREPOSITORY_HPP
