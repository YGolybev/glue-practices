//
// Created by Юрий Голубев on 15.01.18.
//

#define CATCH_CONFIG_MAIN

#include <cstdio>
#include <cstring>
#include <iostream>
#include "FlightRepository.hpp"
#include "catch1.hpp"

using namespace std;

void makeTestFileEmpty(string testFileName) {
    ofstream testTmp(testFileName, ios::out | ios::trunc);
    testTmp.flush();
    testTmp.close();
}

void putFourFlightsInTestFile(string testFileName) {
    makeTestFileEmpty(testFileName);

    flight f1;
    flight f2;
    flight f3;
    flight f4;

    f1.departureTime = 1;
    f2.departureTime = 2;
    f3.departureTime = 3;
    f4.departureTime = 4;

    f1.arrivalTime = 2;
    f2.arrivalTime = 3;
    f3.arrivalTime = 4;
    f4.arrivalTime = 5;

    f1.departureAirport = "da1";
    f2.departureAirport = "da2";
    f3.departureAirport = "da3";
    f4.departureAirport = "da4";

    f1.arrivalAirport = "da4";
    f2.arrivalAirport = "da3";
    f3.arrivalAirport = "da2";
    f4.arrivalAirport = "da1";

    f1.planeManufacturer = "ma1";
    f2.planeManufacturer = "ma2";
    f3.planeManufacturer = "ma3";
    f4.planeManufacturer = "ma4";

    FlightRepository flightRepository(&testFileName);

    flightRepository.add(f1);
    flightRepository.add(f2);
    flightRepository.add(f3);
    flightRepository.add(f4);
}

TEST_CASE("Flight cmp works positive") {
    flight f1;
    f1.arrivalTime = 1;
    f1.departureTime = 2;
    f1.departureAirport = "air1";
    f1.arrivalAirport = "airarr1";
    f1.planeManufacturer = "manuf1";

    flight f2;
    f2.arrivalTime = 1;
    f2.departureTime = 2;
    f2.departureAirport = "air1";
    f2.arrivalAirport = "airarr1";
    f2.planeManufacturer = "manuf1";

    REQUIRE(f1.equ(f2));
    REQUIRE(f1.equ(f1));
    REQUIRE(f2.equ(f1));
    REQUIRE(f2.equ(f2));
}

TEST_CASE("Flight cmp works negative") {
    flight f1;
    f1.arrivalTime = 1;
    f1.departureTime = 2;
    f1.departureAirport = "air1";
    f1.arrivalAirport = "airarr1";
    f1.planeManufacturer = "manuf1";

    flight f2;
    f2.arrivalTime = 3;
    f2.departureTime = 4;
    f2.departureAirport = "aaaa";
    f2.arrivalAirport = "airarr1";
    f2.planeManufacturer = "manuf1";

    REQUIRE_FALSE(f1.equ(f2));
    REQUIRE_FALSE(f2.equ(f1));
}

TEST_CASE("Empty File contains nothing") {
    string testFileName("test_file_tmp.txt");

    makeTestFileEmpty(testFileName);

    FlightRepository repository(&testFileName);

    REQUIRE(repository.getSize() == 0);

}

TEST_CASE("Get size returns size equal to add count") {
    string testFileName("test_file_tmp.txt");

    putFourFlightsInTestFile(testFileName);

    FlightRepository flightRepository(&testFileName);

    REQUIRE(flightRepository.getSize() == 4);

    makeTestFileEmpty(testFileName);
}

TEST_CASE("All adds are returned by get") {
    string testFileName("test_file_tmp.txt");

    putFourFlightsInTestFile(testFileName);

    FlightRepository flightRepository(&testFileName);

    REQUIRE(flightRepository.get(0).second);
    REQUIRE(flightRepository.get(1).second);
    REQUIRE(flightRepository.get(2).second);
    REQUIRE(flightRepository.get(3).second);

    makeTestFileEmpty(testFileName);
}

TEST_CASE("Non existent adds not returned by get") {
    string testFileName("test_file_tmp.txt");

    putFourFlightsInTestFile(testFileName);

    FlightRepository flightRepository(&testFileName);

    REQUIRE_FALSE(flightRepository.get(4).second);
    REQUIRE_FALSE(flightRepository.get(112).second);

    makeTestFileEmpty(testFileName);
}

TEST_CASE("Remove removes all specified positions") {
    string testFileName("test_file_tmp.txt");

    putFourFlightsInTestFile(testFileName);

    FlightRepository flightRepository(&testFileName);

    for (int i = 0; i <= 3; i++) {
        flightRepository.remove(0);
    }

    REQUIRE(flightRepository.getSize() == 0);

    makeTestFileEmpty(testFileName);
}

TEST_CASE("Get size returns -1 on non-existent file") {
    string testFileName("test_file_tmp.txt");

    std::remove(testFileName.c_str());

    FlightRepository flightRepository(&testFileName);

    REQUIRE(flightRepository.getSize() == -1);
}

TEST_CASE("Added flight equals to returned") {
    string testFileName("test_file_tmp.txt");

    putFourFlightsInTestFile(testFileName);

    FlightRepository flightRepository(&testFileName);

    pair<flight, bool> oFlight = flightRepository.get(0);

    REQUIRE(oFlight.second);

    flight flight = oFlight.first;

    REQUIRE(flight.departureTime == 1);
    REQUIRE(flight.arrivalTime == 2);
    REQUIRE(strcmp(flight.departureAirport.c_str(), "da1") == 0);
    REQUIRE(strcmp(flight.arrivalAirport.c_str(), "da4") == 0);
    REQUIRE(strcmp(flight.planeManufacturer.c_str(), "ma1") == 0);

    makeTestFileEmpty(testFileName);
}


TEST_CASE("Flight updates") {
    string testFileName("test_file_tmp.txt");

    putFourFlightsInTestFile(testFileName);

    FlightRepository flightRepository(&testFileName);

    flight updatedFlight;
    updatedFlight.arrivalTime = 13;
    updatedFlight.departureTime = 14;
    updatedFlight.departureAirport = "newAir";
    updatedFlight.arrivalAirport = "newAirArrival";
    updatedFlight.planeManufacturer = "newPlaneManufactruer";

    flightRepository.update(0, updatedFlight);

    pair<flight, bool> oFlight = flightRepository.get(0);

    REQUIRE(oFlight.second);

    flight flight = oFlight.first;

    REQUIRE(flight.equ(updatedFlight));

    makeTestFileEmpty(testFileName);
}

TEST_CASE("Full operations test") {
    string testFileName("test_file_tmp.txt");

    makeTestFileEmpty(testFileName);
    
    FlightRepository flightRepository(&testFileName);

    flight f1;
    f1.arrivalTime = 1;
    f1.departureTime = 2;
    f1.departureAirport = "air1";
    f1.arrivalAirport = "airarr1";
    f1.planeManufacturer = "manuf1";

    flight f2;
    f2.arrivalTime = 3;
    f2.departureTime = 4;
    f2.departureAirport = "air2";
    f2.arrivalAirport = "airar2";
    f2.planeManufacturer = "manuf2";
    
    flightRepository.add(f1);
    flightRepository.add(f2);

    REQUIRE(flightRepository.getSize() == 2);
    
    pair<flight, bool> returnedf1 = flightRepository.get(0);
    REQUIRE(returnedf1.second);
    REQUIRE(returnedf1.first.equ(f1));

    pair<flight, bool> returnedf2 = flightRepository.get(1);
    REQUIRE(returnedf2.second);
    REQUIRE(returnedf2.first.equ(f2));
    
    flight updatedFlight;
    updatedFlight.arrivalTime = 13;
    updatedFlight.departureTime = 14;
    updatedFlight.departureAirport = "newAir";
    updatedFlight.arrivalAirport = "newAirArrival";
    updatedFlight.planeManufacturer = "newPlaneManufactruer";

    flightRepository.update(1, updatedFlight);

    pair<flight, bool> oFlightUpdated = flightRepository.get(1);

    REQUIRE(oFlightUpdated.second);
    REQUIRE(oFlightUpdated.first.equ(updatedFlight));

    flightRepository.remove(0);

    REQUIRE(flightRepository.getSize() == 1);
    REQUIRE_FALSE(flightRepository.get(1).second);

    pair<flight, bool> oFlightAfterRemove = flightRepository.get(0);
    REQUIRE(oFlightAfterRemove.second);
    REQUIRE(oFlightAfterRemove.first.equ(updatedFlight));

    makeTestFileEmpty(testFileName);
    
}
