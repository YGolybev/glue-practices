//
// Created by Юрий Голубев on 15.01.18.
//

#include <iostream>
#include <fstream>
#include "FlightRepository.hpp"

using namespace std;

void flight::show() {
    cout << endl;
    cout << "arrivalTime: " << arrivalTime << endl;
    cout << "departureTime: " << departureTime << endl;
    cout << "arrivalAirport: " << arrivalAirport << endl;
    cout << "departureAirport: " << departureAirport << endl;
    cout << "manufacturer: " << planeManufacturer << endl;
}

bool flight::equ(flight other) {
    return departureTime == other.departureTime &&
           arrivalTime == other.arrivalTime &&
           (strcmp(departureAirport.c_str(), other.departureAirport.c_str()) == 0) &&
           (strcmp(arrivalAirport.c_str(), other.arrivalAirport.c_str()) == 0) &&
           (strcmp(planeManufacturer.c_str(), other.planeManufacturer.c_str()) == 0);
}

void FlightRepository::add(flight flight) {
    ofstream dbFile(databaseFilename, ios::out | ios::app);

    if (dbFile.good()) {
        dbFile.write((char *) &flight, sizeof(flight));
    }

    dbFile.close();
}

void FlightRepository::update(int position, flight flight) {
    fstream dbFile(databaseFilename, ios::out | ios::in);

    if (dbFile.good()) {
        const long positionInFile = position * sizeof(flight);

        dbFile.seekp(positionInFile);

        dbFile.write((char *) &flight, sizeof(flight));

        dbFile.flush();
    }

    dbFile.close();
}

void FlightRepository::remove(int position) {
    string tmpName = databaseFilename + "_tmp";

    ofstream temporaryDbFile(tmpName, ios::out | ios::app);

    pair<flight, bool> currentFlight;

    if (temporaryDbFile.good()) {
        unsigned int currentPos = 0;

        do {
            currentFlight = get(currentPos);

            if (currentFlight.second && currentPos != position) {
                temporaryDbFile.write((char *) &currentFlight, sizeof(flight));
            }

            currentPos += 1;
        } while (currentFlight.second);
    }

    temporaryDbFile.flush();
    temporaryDbFile.close();

    if (std::remove(databaseFilename.c_str()) != 0) {
        cout << "ERROR while removing" << endl;
    } else {
        if (std::rename(tmpName.c_str(), databaseFilename.c_str()) != 0) {
            cout << "ERROR while renaming" << endl;
        }
    }
}

pair<flight, bool> FlightRepository::get(int position) {
    ifstream dbFile(databaseFilename, ios::in);

    const bool valid = dbFile.good();

    flight buf;

    int curIndex = 0;

    if (valid) {
        while (!dbFile.eof() && curIndex <= position) {
            dbFile.read((char *) &buf, sizeof(buf));

            curIndex += 1;
        }
    }

    const bool isFound = curIndex - 1 == position && !dbFile.eof() && valid;

    dbFile.close();

    return pair<flight, bool>(buf, isFound);
}

const long FlightRepository::getSize() {
    ifstream dbFile(databaseFilename, ios::in);

    long size;

    if (dbFile.good()) {
        dbFile.seekg(0, dbFile.end);

        size = dbFile.tellg() / sizeof(flight);
    } else {
        size = -1;
    }

    dbFile.close();

    return size;
}