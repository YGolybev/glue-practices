#include <cstdio>
#include <cstring>
#include <iostream>
#include "FlightRepository.hpp"
#include "FlightInput.hpp"

using namespace std;

void clrscr() {
    system("clear");
}

void add(FlightRepository flightRepository) {
    string userInput;

    flight newFlight = inputNewFlight();
    newFlight.show();

    cout << "ITS OKAY, save? Y/N";
    getline(cin, userInput);

    if (strcasecmp(userInput.c_str(), string("Y").c_str()) == 0) {
        flightRepository.add(newFlight);
    }

    cout << endl;
}

void edit(FlightRepository flightRepository) {
    cout << "EDIT MODE: t - edit cur, d - delete cur, p - previous, n - next, e - exit\n";

    pair<flight, bool> tmp;
    unsigned int currentPos = 0;
    long maxSize = flightRepository.getSize();

    string userInput;

    getline(cin, userInput);

    do {
        tmp = flightRepository.get(currentPos);

        if (tmp.second) {
            tmp.first.show();
        } else {
            cout << "ELEMENT NOT FOUND ";
        }

        cout << "is it what you want?";
        getline(cin, userInput);

        if (strcmp(userInput.c_str(), string("p").c_str()) == 0) {
            if (currentPos > 0) {
                currentPos -= 1;
            } else {
                cout << "THIS IS FIRST ELEMENT THERE'S NOWHERE TO MOVE" << endl;
            }
        } else if (strcmp(userInput.c_str(), string("n").c_str()) == 0) {
            if (currentPos < maxSize) {
                currentPos += 1;
            } else {
                cout << "THIS IS LAST ELEMENT THERE'S NOWHERE TO MOVE" << endl;
            }
        } else if (strcmp(userInput.c_str(), string("t").c_str()) == 0) {
            flight newFlight = inputNewFlight();
            newFlight.show();

            if (currentPos < flightRepository.getSize()) {
                flightRepository.update(currentPos, newFlight);
            } else {
                cout << "SOMETHING IS WRONG WITH SIZE" << endl;
            }
        } else if (strcmp(userInput.c_str(), string("d").c_str()) == 0) {
            if (currentPos < flightRepository.getSize()) {
                flightRepository.remove(currentPos);
            } else {
                cout << "SOMETHING IS WRONG" << endl;
            }
        } else {
            break;
        }

        cout << endl;
    } while (strcmp(userInput.c_str(), string("e").c_str()) != 0);

    clrscr();
}

void getAll(FlightRepository &flightRepository) {
    pair<flight, bool> currentFlight;
    unsigned int currentPos = 0;

    do {
        currentFlight = flightRepository.get(currentPos);

        currentPos += 1;

        if (currentFlight.second) {
            currentFlight.first.show();
        }
    } while (currentFlight.second);

    cout << "THAT'S ALL FOLKS!\n";
}

int main() {
    clrscr();

    string fileName;

    char menu[] = "1 - ADD\n2 - EDIT\n3 - GET ALL\n4 - TERMINATE\n";
    int c = 0;

    const int CMD_TERMINATE = 4;

    cout << "ENTER DATABASE FILENAME: ";

    getline(cin, fileName);

    FlightRepository flightRepository(&fileName);

    clrscr();
    cout << "DATABASE IS: " + fileName + "\n";
    cout << menu;

    do {
        scanf("%d", &c);

        switch (c) {
            case 1:
                clrscr();
                add(flightRepository);
                cout << menu;
                break;

            case 2:
                clrscr();
                edit(flightRepository);
                cout << menu;
                break;

            case 3:
                clrscr();
                getAll(flightRepository);
                cout << menu;
                break;

            case CMD_TERMINATE:
                cout << "))))\n";
                break;

            default:
                clrscr();
                cout << "ne lez'\n";
                cout << menu;
        }
    } while (c != CMD_TERMINATE);

    return 1;
}